package ru.mephi.repository;

import ru.mephi.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRep extends JpaRepository<Role, Integer> {

}
