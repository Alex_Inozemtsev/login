package ru.mephi.repository;

import ru.mephi.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRep extends JpaRepository<User, Long> {
    User findByUsername (String username);
}
