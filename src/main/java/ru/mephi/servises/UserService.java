package ru.mephi.servises;

import ru.mephi.models.Role;
import ru.mephi.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.mephi.repository.RoleRep;
import ru.mephi.repository.UserRep;

import java.util.Collections;
import java.util.List;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserRep userRep;

    @Autowired
    RoleRep roleRep;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;




    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRep.findByUsername(s);

        if (user == null) {
            throw new UsernameNotFoundException("User" + s + "not found");
        }

        return  user;
    }

    public boolean  saveUser(User user) {
        User existingUser = userRep.findByUsername(user.getUsername());

        if (existingUser != null) {
            return false;
        }

        user.setRoles(Collections.singleton(new Role(2, "ROLE_USER") ));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRep.save(user);
        return true;
    }

    public boolean deleteUser(Long id) {
        if (userRep.findById(id).isPresent()) {
            userRep.deleteById(id);
            return true;
        }
        return false;
    }

    public List<User> allUsers() {
        return userRep.findAll();
    }
}
