package ru.mephi.controllers;

import ru.mephi.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.mephi.servises.UserService;

@Controller
public class RegistrationController {
    @Autowired
    UserService us;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userData", new User());
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(
            @ModelAttribute("userData") User userData,
            Model model) {

        if (!userData.getPassword().equals(userData.getPasswordConf())){
            model.addAttribute("passwordError", "Passwords do not match");
            return "registration";
        }

        if (!us.saveUser(userData)){
            model.addAttribute("usernameError", "User already exists");
            return "registration";
        }

        return "redirect:/";
    }
}
